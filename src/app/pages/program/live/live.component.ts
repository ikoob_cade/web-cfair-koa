import * as _ from 'lodash';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { AgendaService } from 'src/app/services/api/agenda.service';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';
import { map } from 'rxjs/operators';
import { MemberService } from 'src/app/services/api/member.service';
import { HistoryService } from 'src/app/services/api/history.service';
import { SocketService } from 'src/app/services/socket/socket.service';
import { BannerService } from 'src/app/services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';

declare var $: any;

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit, OnDestroy {
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;

  public user: any; // 사용자 정보
  public live: any; // 라이브 방송
  public isLiveAgenda: any; // 현재 진행중인 아젠다
  public attendance: boolean; // 출석상태

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public allAgendas: any[] = []; // 모든 강의
  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.

  public selected: any = {
    date: null,
    room: null
  };

  public showPlayer = false;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private dateService: DateService,
    private roomService: RoomService,
    private agendaService: AgendaService,
    private memberService: MemberService,
    private historyService: HistoryService,
    private socketService: SocketService,
    private bannerService: BannerService
  ) {
    this.doInit();
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
  }

  doInit(): void {
    this.historyService.setAttendance('in');

    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          // this.allAgendas = resp[0];
          this.dates = resp[0];
          this.rooms = resp[1];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[0];

        let room = this.rooms[0];
        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId && param.roomId) {
            date = this.dates.find(date => { return date.id === param.dateId; });
            room = this.rooms.find(room => { return room.id === param.roomId; });
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });

        // this.getAgendasTest(date.id, room.id).subscribe(agendas => {
        //   this.allAgendas = agendas;
        // });

        this.selected = { date: date, room: room };
        this.setAgendaList(date, room);
      });
  }

  public next: any;
  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean): any {
    /*
    * 날짜의 옵션값 확인해서 player컴포넌트 출력을 제어한다.
    * 선택한 날짜의 ON/OFF 상태를 확인해서 플레이어, 채팅 이용 불가하도록 하기 위함
    */
    if (!date.showPlayer) {
      this.showPlayer = false;
    } else {
      this.showPlayer = true;
    }

    // 소켓 접속
    setTimeout(() => {
      this.socketService.leave(this.selected.room.id);
      this.socketService.join(room.id);
    }, this.user ? 0 : 300);

    if (this.live && !this.historyService.isAttended()) {

      // 입장
      this.openEntryAlert();
      if (!this.showPlayer) {
        this.showPlayer = !this.showPlayer;
      }

      this.next = () => { this.setAgendaList(date, room, true); };
    } else {
      // 입장/퇴장
      if (next) {
        this.next = null;
      }
      let num = next ? 500 : 0;

      setTimeout(() => {
        this.selected = {
          date,
          room
        };

        this.getAgendasV2(date.id, room.id).subscribe(agendas => {
          this.agendas = agendas;

          setTimeout(() => {
            this.live = this.selected.room.contents;

            // Live가 있으면 IN / OUT
            if (this.live) {
              if (this.showPlayer) {
                this.openEntryAlert();
              }

              this.checkLiveAgenda();
            }
          }, 100);
        });

        // this.agendas = this.allAgendas.filter(agenda => {
        //   return date.id === agenda.date.id && room.id === agenda.room.id;
        // });

        this.live = null; // new app-player
      }, 500);
    }
  }

  /**
   * 입장/퇴장 모달을 출력한다.
   * user의 isLog데이터 구분.
   * 해외연자는 연수평점의 의무가 없어서 입장/퇴장이 불필요하다.
   */
  openEntryAlert(): void {
    // 로그 저장하지 않는 경우
    if (!this.user.isLog) {
      return;
    }
    if (true) {
      this.entryAlertBtn.nativeElement.click();
    }
  }

  // 현재 진행중인 아젠다 확인
  checkLiveAgenda(): void {
    this.isLiveAgenda = this.agendas[0];
    this.agendas.forEach((agenda: any) => {
      const agendaDate = agenda.date.date.replace(/-/gi, '/');
      const agendaStartTime = new Date(`${agendaDate}/${agenda.startTime}`);
      const agendaEndTime = new Date(`${agendaDate}/${agenda.endTime}`);
      const now: Date = new Date();
      if (now > agendaStartTime && now < agendaEndTime) {
        this.isLiveAgenda = agenda;
      }
    });

  }

  /** 아젠다 목록 조회 */
  getAgendas(): Observable<any> {
    return this.agendaService.find();
  }

  getAgendasV2(dateId, roomId): Observable<any> {
    return this.agendaService.findV2(dateId, roomId);
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * 강의 상세보기
   * @param agenda 강의
   */
  goDetail(agenda: any): void {
    this.router.navigate([`/live/${this.selected.date.id}/${this.selected.room.id}/${agenda.id}`]);
  }

  // 출석체크
  atndnCheck(isMove?): void {
    if (!this.user.isLog) {
      // 로그 저장 안함
      return;
    }

    const options: { relationId: string, relationType: string, logType: string } = {
      relationId: this.selected.room.id,
      relationType: 'room',
      logType: this.historyService.getAttendance()
    };

    localStorage.setItem('currentRoom', this.selected.room.id);
    this.memberService.history(this.user.id, options)
      .subscribe(
        (resp) => {
          if (this.historyService.isAttended()) {
            this.historyService.setAttendance('out');
          } else {
            this.historyService.setAttendance('in');
          }
          if (this.next) {
            this.next();
          }

          if (isMove) {
            this.openEntryAlert();
          }
        },
        (error) => {
          console.error(error);
        });
  }

  // 참여 상태 출력
  getAttendance(): string {
    return this.historyService.isAttended() ? 'in' : 'out';
  }

  /**
   * 페이지 나갈 때 처리
   */
  ngOnDestroy(): void {
    this.socketService.leave(this.selected.room.id);
  }

  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) history('out')을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {

    // 로그 저장하는 경우
    if (!this.historyService.isAttended() && this.user.isLog) {
      this.atndnCheck(true);
    }

    $event.preventDefault();
    ($event || window.event).returnValue = '로그아웃 하시겠습니까?';
    return false;
  }
}
