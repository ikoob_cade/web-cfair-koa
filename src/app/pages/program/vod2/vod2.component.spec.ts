import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Vod2Component } from './vod2.component';

describe('VodComponent', () => {
  let component: Vod2Component;
  let fixture: ComponentFixture<Vod2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vod2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Vod2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
