import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, HostListener } from '@angular/core';
import { SpeakerService } from 'src/app/services/api/speaker.service';
import { BoothService } from 'src/app/services/api/booth.service';
// import { SponsorService } from 'src/app/services/api/sponsor.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public ebookUrl = 'https://cfair-files.s3.ap-northeast-2.amazonaws.com/events/5f696f7988bf9a001237980e/2020%2B%E1%84%8C%E1%85%A5%E1%86%BC%E1%84%92%E1%85%A7%E1%86%BC%E1%84%8B%E1%85%AC%E1%84%80%E1%85%AA%2B%E1%84%80%E1%85%AE%E1%86%A8%E1%84%8C%E1%85%A6%E1%84%92%E1%85%A1%E1%86%A8%E1%84%89%E1%85%AE%E1%86%AF%E1%84%83%E1%85%A2%E1%84%92%E1%85%AC%2B%E1%84%91%E1%85%B3%E1%84%85%E1%85%A9%E1%84%80%E1%85%B3%E1%84%85%E1%85%A2%E1%86%B7%E1%84%87%E1%85%AE%E1%86%A8.pdf';

  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public booths: Array<any> = []; // 카테고리[부스] 목록
  public selectedBooth: any;
  public mobile: boolean;
  public excerptUrl = ''; // TODO ICDM 초록집 URL 기입

  public attachments = []; // 부스 첨부파일

  constructor(
    private speakerService: SpeakerService,
    // private sponsorService: SponsorService,
    private boothService: BoothService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.loadSpeakers();
    this.loadBooths();
    // this.loadSponsors();
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    let limit = 6;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) { limit = 6; }
    this.speakerService.find().subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  /**
   * 스폰서 목록 조회
   */
  // loadSponsors = () => {
  //   this.sponsorService.find().subscribe(res => {
  //     // console.log('GET Sponsors', res);
  //     this.sponsors = res;
  //   });
  // }

  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      console.log('GET Booths', res);
      this.booths = res;

      // const categories =
      //   _.chain(res)
      //     .groupBy(booth => {
      //       return booth.category ? JSON.stringify(booth.category) : '{}';
      //     })
      //     .map((booth, category) => {
      //       category = JSON.parse(category);
      //       category.booths = booth;

      //       // console.log('category= ', category)
      //       return category;
      //     }).sortBy(category => {
      //       return category.seq;
      //     })
      //     .value();

      // this.booths = categories;
    });
  }


  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  // 부스 상세보기
  getDetail = (selectedBooth) => {
    this.boothService.findOne(selectedBooth.id).subscribe(res => {
      this.selectedBooth = res;
      this.setAttachments();
      this.setDesc();
    });
  }

}
