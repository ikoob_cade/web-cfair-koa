import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;

  private user: any;
  public passwordError = ' '; // 로그인 에러메세지

  public member = {
    email: '',
    password: '',
  };

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {
  }

  login(): void {
    this.auth.login(this.member).subscribe(res => {
      if (res.token) {
        this.user = res;
        this.passwordError = '';
        this.policyAlertBtn.nativeElement.click();
      }
    }, error => {
      if (error.status === 401) {
        this.passwordError = '로그인 정보가 일치하지 않습니다.';
      }
      if (error.status === 404) {
        this.passwordError = '로그인 정보를 찾을 수 없습니다.';
      }
    });
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    localStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }
}
