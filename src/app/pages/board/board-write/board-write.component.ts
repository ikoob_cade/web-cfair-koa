import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BoardService } from 'src/app/services/api/board.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-board-write',
  templateUrl: './board-write.component.html',
  styleUrls: ['./board-write.component.scss']
})
export class BoardWriteComponent implements OnInit {

  public user: any;
  public formGroup: FormGroup;
  public board: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private boardService: BoardService
  ) { }

  ngOnInit(): void {
    this.setFormGroup();
    if (this.route.snapshot.queryParams.boardId) {
      let boardId = this.route.snapshot.queryParams.boardId;
      this.getBoardById(boardId);
    }
  }

  getBoardById(boardId: string) {
    this.boardService.findOne(boardId)
      .subscribe((resp: any) => {
        this.board = resp;
        this.formGroup.patchValue({
          title: resp.title,
          content: resp.content,
          type: resp.type,
          isFixed: resp.isFixed,
          writer: resp.writer,
          phone: resp.phone,
          email: resp.email,
          password: resp.password,
        });
      });
  }

  setFormGroup() {
    this.formGroup = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      content: ['', Validators.compose([Validators.required])],
      type: ['post', Validators.compose([Validators.required])],
      isFixed: [false],
      writer: [''],
      phone: [''],
      email: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')])],
      password: ['', Validators.compose([Validators.minLength(2)])],
    })
    // 유저 정보기 있을 경우
    if (localStorage.getItem('cfair')) {
      this.user = JSON.parse(localStorage.getItem('cfair'));
      this.formGroup.patchValue({
        writer: this.user.name,
        email: this.user.email
      });
    }
  }

  // 글 작성 완료
  submit() {
    if (this.formGroup.invalid) {
      let target: string;
      if (this.formGroup.controls.content.invalid) target = '내용';
      if (this.formGroup.controls.title.invalid) target = '제목';
      return alert(`${target}을 입력해주세요.`);
    } else {
      (this.board && this.board.id) ? this.update() : this.create();
    }
  }

  // 글 추가하기
  create() {
    this.boardService.create(this.formGroup.value)
      .subscribe((resp: any) => {
        this.router.navigate(['/board']);
      });
  }

  // 글 수정하기
  update() {
    this.boardService.update(this.board.id, this.formGroup.value)
      .subscribe((resp: any) => {
        this.router.navigate(['/board']);
      });
  }

  // 글 작성 취소
  cancelWrite() {
    let result = confirm('작성을 취소하시겠습니까?');
    if (result) {
      this.router.navigate(['/bloard'])
    }
  }

}
