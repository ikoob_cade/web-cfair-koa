import * as _ from 'lodash';
import { Component, OnInit, HostListener } from '@angular/core';
import { BoothService } from 'src/app/services/api/booth.service';
import { _ParseAST } from '@angular/compiler';
import { SponsorService } from 'src/app/services/api/sponsor.service';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent implements OnInit {

  public sponsors: Array<any> = []; // 스폰서 목록
  public mobile: boolean = false;

  constructor(
    private sponsorService: SponsorService,
  ) { }

  ngOnInit(): void {
    this.loadSponsors();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobile = window.innerWidth < 768;
  }

  /**
   * 스폰서 목록 조회
   */
  loadSponsors = () => {
    this.sponsorService.find().subscribe(res => {
      // console.log('GET Sponsors', res);
      this.sponsors = res;
    });
  }
}
