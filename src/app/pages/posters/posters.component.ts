import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { PosterService } from 'src/app/services/api/poster.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-posters',
  templateUrl: './posters.component.html',
  styleUrls: ['./posters.component.scss']
})
export class PostersComponent implements OnInit {

  public allPosters: Array<any>; // 전체 포스터 목록
  public allCategories: Array<any>; // 전체 카테고리 목록
  public categories: Array<any>; // 카테고리별 포스터 목록
  public posters: Array<any>; // 포스터 목록
  public searchText: string; // 검색어
  public searchType: string; // 검색 카테고리

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private posterService: PosterService,
  ) {
    this.searchType = '';
  }

  ngOnInit(): void {
    this.loadPosters();
  }

  /**
   * 포스터 목록 조회
   */
  loadPosters = () => {
    this.posterService.find().subscribe(res => {
      // console.log('GET Posters', res);

      const filteredPosters = _.sortBy(res, 'subTitle'); // ex) PE01

      this.allPosters = filteredPosters;
      this.posters = filteredPosters;
      this.categories = this.sortByCategory(filteredPosters);

      this.allCategories = this.categories;
      // console.log(this.categories);
    });
  }

  /**
   * 포스터 목록을 카테고리 별로 정제한다.
   * @param posters 포스터 목록
   */
  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.posters = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

  /** 카테고리를 선택한다. */
  selectCategory = (category) => {
    this.posters = category === 'all' ? this.allPosters : category.posters;
  }

  /** 카테고리 검색 */
  search = () => {
    const searchObject = this.searchType ? _.filter(this.allPosters, poster => {
      if (!poster.category) {
        return false;
      }
      return poster.category.categoryName === this.searchType;
    }) : this.allPosters;

    if (this.searchText) {
      this.posters = _.filter(searchObject, poster => {
        return poster.title.toLowerCase().includes(this.searchText.toLowerCase());
      });
    } else {
      this.posters = searchObject;
    }

    this.categories = this.sortByCategory(this.posters);
    // console.log(this.posters);
  }

  /** 포스터 상세로 페이지 이동 */
  goDetail(posterId: string): void {
    this.router.navigate([`posters/${posterId}`]);
  }

}
