import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HistoryService } from '../api/history.service';
import { MemberService } from '../api/member.service';

@Injectable({
  providedIn: 'root'
})
export class AttendGuard implements CanActivate {

  constructor(
    private router: Router,
    private historyService: HistoryService,
    private memberService: MemberService,
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // console.log('next', next);

    // 라이브에서 퇴실 상황인 경우
    if (!this.historyService.isAttended()) {
      if (confirm('현재 시청중입니다. 퇴장 하시겠습니까?')) {
        const options: { relationId: string, relationType: string, logType: string } = {
          relationId: localStorage.getItem('currentRoom'),
          relationType: 'room',
          logType: this.historyService.getAttendance()
        };

        const user = JSON.parse(localStorage.getItem('cfair'));
        // 서버에 저장
        this.memberService.history(
          user.id,
          options).subscribe(res => {
            localStorage.removeItem('currentRoom');
          });

        this.historyService.setAttendance(null);

        // this.router.navigate([next.routeConfig.path]);
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

}
