import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as io from 'socket.io-client';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { HistoryService } from '../api/history.service';
import { MemberService } from '../api/member.service';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  // private url = 'ws://192.168.1.3:38038'; // dev
  private url = 'https://cfair-socket-ssl.cf-air.com'; // 운영
  private socket;
  private user;
  private currentRoomId;
  private memberId_live;
  public receiveMessage = new BehaviorSubject(false);

  constructor(
    public router: Router,
    private historyService: HistoryService,
    private memberService: MemberService,
  ) {
  }

  init(): void {
    this.socket = io(this.url, {
      transports: ['websocket']
    });

    this.user = JSON.parse(localStorage.getItem('cfair'));
    // console.log(this.user);
    // 소켓 연결
    this.socket.on('connect', () => {

      if (this.user) {
        this.login({
          memberId: this.user.id,
          token: this.user.token
        });
      }
      if (this.currentRoomId) {
        this.join(this.currentRoomId);
      }
      // console.log('connected');
    });

    // 소켓 연결 해제
    this.socket.on('disconnect', () => {
      // console.log('disconnected');
    });

    // 중복 로그인
    this.socket.on('new_login', (data) => {
      // console.log('new_login', data);
      this.logout({
        memberId: data.memberId
      });

      /**
       * 라이브에서 중복로그인 발생 시
       * 튕기기 전에 history-out 전송해야 하기때문에 memberId_live에 임시로 저장한다.
       */
      this.memberId_live = data.memberId;
      if (!this.historyService.isAttended()) {
        const options: { relationId: string, relationType: string, logType: string } = {
          relationId: localStorage.getItem('currentRoom'),
          relationType: 'room',
          logType: this.historyService.getAttendance()
        };
        // 서버에 저장
        this.memberService.history(
          this.memberId_live,
          options).subscribe(res => {
            localStorage.removeItem('currentRoom');
          });

        this.historyService.setAttendance(null);
      }

      // out 후에 토큰 remove
      localStorage.removeItem('cfair');

      alert('다른 기기에서 로그인하여\n자동으로 로그아웃 되었습니다.');
      this.router.navigate(['/']);
    });
  }

  getUser(): void {
    this.user = JSON.parse(localStorage.getItem('cfair'));
  }

  login(data): void {
    this.socket.emit('login', data, (result) => {
      // console.log(result);
    });
  }

  /**
   * 로그아웃 전달
   */
  logout(data): void {
    this.socket.emit('logout', data, (result) => {
      // console.log(result);
      this.currentRoomId = null;
    });
  }

  /**
   * Room 입장
   * @param roomId 룸 ID
   */
  join(roomId): void {
    this.getUser();

    this.socket.emit('join', {
      id: roomId,
      type: 'room',
      memberId: this.user.id
    }, (result) => {
      // console.log(result);
    });

    this.currentRoomId = roomId;
  }

  /**
   * Room 퇴장
   * @param roomId 룸 ID
   */
  leave(roomId): void {
    this.getUser();
    // console.log(this.memberId_live);

    // live에서 퇴장 시 user정보가 소실되었기 때문에 임시로 저장해 둔 memberId_live 이용해서 소켓에 leave를 알려준다.
    this.socket.emit('leave', {
      id: roomId,
      type: 'room',
      memberId: this.memberId_live ? this.memberId_live : this.user.id,
    });

    // 값 비워주기
    this.memberId_live = null;
    this.currentRoomId = null;
  }

}
