import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class VodService {
  private serverUrl = '/events/:eventId/vod';

  constructor(private http: HttpClient) { }

  find(pageOption?: string): Observable<any> {
    let params = {};
    if (pageOption) {
      params = {
        params: {
          pageOption
        }
      };
    }
    return this.http.get(this.serverUrl, params)
      .pipe(catchError(this.handleError));
  }

  findOne(vodId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + vodId)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}
