import { DeviceDetectorService } from 'ngx-device-detector';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, COMPOSITION_BUFFER_MODE } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MomentModule } from 'ngx-moment';
import { SocketService } from './services/socket/socket.service';
import { NgImageSliderModule } from 'ng-image-slider';

// Service
import { ApiService } from './services/api/api.service';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptorService } from './services/auth-interceptor/auth-interceptor.service';
import { SpeakerService } from './services/api/speaker.service';
import { AgendaService } from './services/api/agenda.service';
import { EventService } from './services/api/event.service';
import { DateService } from './services/api/date.service';
import { RoomService } from './services/api/room.service';
import { PosterService } from './services/api/poster.service';
import { VodService } from './services/api/vod.service';
import { SponsorService } from './services/api/sponsor.service';
import { BoothService } from './services/api/booth.service';
import { MemberService } from './services/api/member.service';
import { BoardService } from './services/api/board.service';
import { BannerService } from './services/api/banner.service';

// Pages
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { BannerComponent } from './components/banner/banner.component';
import { AboutComponent } from './pages/about/about.component';
import { MyPageComponent } from './pages/my-page/my-page.component';

import { ProgramVodComponent } from './components/program-vod/program-vod.component';
import { AgendaComponent } from './components/agenda/agenda.component';

import { SpeakersComponent } from './pages/speakers/speakers.component';

import { PostersComponent } from './pages/posters/posters.component';
import { PostersDetailComponent } from './pages/posters/posters-detail/posters-detail.component';

// import { SponsorsComponent } from './pages/sponsors/sponsors.component';
import { EBoothComponent } from './pages/e-booth/e-booth.component';

import { BoardComponent } from './pages/board/board.component';
import { BoardDetailComponent } from './pages/board/board-detail/board-detail.component';
import { BoardWriteComponent } from './pages/board/board-write/board-write.component';

import { RegistraionComponent } from './pages/registraion/registraion.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

// Components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PlayerComponent } from './components/player/player.component';
import { PlayerVodComponent } from './components/player-vod/player-vod.component';
import { TimeTableComponent } from './components/time-table/time-table.component';
import { ProgramNavComponent } from './components/program-nav/program-nav.component';

import { ProgramComponent } from './pages/program/program.component';
import { LiveComponent } from './pages/program/live/live.component';
import { VodComponent } from './pages/program/vod/vod.component';
import { Vod2Component } from './pages/program/vod2/vod2.component';
import { AgendaInfoComponent } from './pages/program/agenda-info/agenda-info.component';
import { SafeHtmlPipe } from './pipes/safe-html/safe-html.pipe';
import { SpeakerComponent } from './components/speaker/speaker.component';
import { ProgramLiveComponent } from './components/program-live/program-live.component';
import { VodDetailComponent } from './pages/program/vod-detail/vod-detail.component';
import { HistoryService } from './services/api/history.service';
import { NotSupportIeComponent } from './pages/not-support-ie/not-support-ie.component';
import { SponsorComponent } from './components/sponsor/sponsor.component';
import { SessionComponent } from './components/session/session.component';


const pages = [
  MainComponent,
  LoginComponent,
  RegistraionComponent,
  PageNotFoundComponent,
  AboutComponent,
  SpeakersComponent,
  PostersComponent,
  PostersDetailComponent,
  EBoothComponent,
  ProgramComponent,
  LiveComponent,
  VodComponent,
  Vod2Component,
  VodDetailComponent,
  AgendaInfoComponent,
  MyPageComponent,
  SpeakerComponent,
  BoardComponent,
  BoardDetailComponent,
  BoardWriteComponent,
  EBoothComponent,
]

const components = [
  HeaderComponent,
  FooterComponent,
  BannerComponent,
  PlayerComponent,
  PlayerVodComponent,
  TimeTableComponent,
  AgendaComponent,
  ProgramNavComponent,
  ProgramVodComponent,
  ProgramLiveComponent,
  NotSupportIeComponent,
  SessionComponent,
  SponsorComponent,
]

const pipes = [
  SafeHtmlPipe,
]

@NgModule({
  declarations: [
    AppComponent,
    ...pages,
    ...components,
    ...pipes,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MomentModule,
    NgImageSliderModule,
  ],
  providers: [
    ApiService,
    AuthService,
    BannerService,
    DatePipe,
    SpeakerService,
    AgendaService,
    EventService,
    DateService,
    RoomService,
    PosterService,
    VodService,
    SponsorService,
    BoothService,
    MemberService,
    BoardService,
    HistoryService,
    DeviceDetectorService,
    SocketService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    { provide: COMPOSITION_BUFFER_MODE, useValue: false },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
