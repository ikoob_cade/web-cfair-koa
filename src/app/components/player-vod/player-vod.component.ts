import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, OnDestroy, ViewEncapsulation, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from 'src/app/services/api/member.service';
import videojs from 'video.js';
import * as _ from 'lodash';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { SocketService } from 'src/app/services/socket/socket.service';
import { Subscription } from 'rxjs';
import { BannerService } from 'src/app/services/api/banner.service';

@Component({
  selector: 'app-player-vod',
  templateUrl: './player-vod.component.html',
  styleUrls: ['./player-vod.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerVodComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @Input('content') content: any; // 전달받은 콘텐츠 정보
  @Input('vodId') vodId: any; // 전달받은 vodId
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보
  @ViewChild('commentList') commentList: any; // 댓글 목록

  public isLive = false;
  public banner;

  public user: any;
  public videoPlayerObject;
  public reg = /vimeo.com/;
  public player: any;

  public replyForm: FormGroup;
  public replys: any = [];
  private relationId: string;

  private timerID;
  private vodWatch;
  public watchTime = 0;

  private isFirstPlay = true;

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    public sanitizer: DomSanitizer,
    private bannerService: BannerService,
  ) {
    this.replyForm = fb.group({
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.timerID);
    clearInterval(this.vodWatch);
    if (this.watchTime > 0) {
      this.insertHistory();
    }
  }

  ngOnInit(): void {
    this.relationId = this.content.id;

    this.user = JSON.parse(localStorage.getItem('cfair'));
    this.getComment();
    this.timerID = setInterval(() => {
      this.getComment();
    }, 10 * 1000);
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    this.setVideoPlayer();
  }

  /** 비디오 플레이어 셋팅 */
  setVideoPlayer(): void {
    const opt: any = {
      controls: false,
      controlBar: {
        subsCapsButton: false,
        subtitlesButton: false,
        pictureInPictureToggle: false,
      },
      fluid: true,
      html5: { nativeTextTracks: false },
      preload: 'auto',
      thumbnail: this.content.thumbNailUrl,
      sources: {
        src: this.content.contentUrl,
        type: 'video/vimeo'
      },
    };

    this.player = new videojs(this.videoPlayer.nativeElement, opt, () => {
      const that = this;
      this.player.on('error', (error) => {
        // console.log('error');
        this.offVideoEvent();
      });

      // 동영상 재생 위치 변경 감지
      this.player.on('loadeddata', () => {
        // console.log('loadeddata');
        that.player.off('loadeddata');
      });

      // 플레이어 초기 지속 시간 발생 감지
      this.player.on('loadedmetadata', () => {
        // console.log('loadedmetadata');
        that.player.off('loadedmetadata');
      });

      // 동영상 종료
      this.player.on('ended', () => {
        clearInterval(this.vodWatch);

        // console.log('ended');
        that.player.off('ended');
        that.player.initChildren();
      });

      // 동영상 재생
      this.player.on('play', () => {
        if (this.user.isLog) {
          if (this.isFirstPlay) {
            this.insertHistory();
            this.isFirstPlay = false;
          }
          
          this.vodWatch = setInterval(() => {
            this.watchTime++;
          }, 1000);
        }
      });

      // 동영상 일시정지
      this.player.on('pause', () => {
        if (this.user.isLog) {
          clearInterval(this.vodWatch);
        }
        // console.log('pause');
      });
    });
  }

  // 비디오 이벤트 제거
  offVideoEvent = () => {
    if (this.player) {
      this.player.off('loaded');
      this.player.off('ended');
      this.player.off('play');
      this.player.off('pause');
      this.player.off('seeked');
      this.player.off('seekable');
      // this.player.off('timeupdate');
    }
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService.findComment(this.user.id, this.relationId).subscribe(res => {
      this.replys = res;
    });
  }

  /** 댓글달기 */
  comment(): void {

    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content,
      relationId: this.relationId
    }).subscribe(res => {
      this.getComment();
      // this.replyForm.value.content = '';
      this.replyForm.patchValue({
        content: ''
      });

      // this.downScroll();
    });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop = this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    if (confirm('삭제하시겠습니까?')) {
      this.memberService.deleteComment(this.user.id, comment.id)
        .subscribe(res => {
          this.getComment();
        });
    }
  }

  // 본 시간(초) 만큼 이력 전송.
  insertHistory(): void {
    const options: { relationId: string, watchTime: number } = {
      relationId: this.vodId,
      watchTime: this.watchTime,
    };

    const user = JSON.parse(localStorage.getItem('cfair'));
    // 서버에 저장
    this.memberService.historyOfVod(
      user.id,
      options).subscribe(res => { });
  }

  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) 시청시간 전송을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {

    // 로그 저장하는 경우
    if (this.watchTime > 0) {
      this.insertHistory();
    }
    this.watchTime = 0;

    $event.preventDefault();
    ($event || window.event).returnValue = '로그아웃 하시겠습니까?';
    return false;
  }
}
