import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramVodComponent } from './program-vod.component';

describe('ProgramVodComponent', () => {
  let component: ProgramVodComponent;
  let fixture: ComponentFixture<ProgramVodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramVodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramVodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
