import * as _ from 'lodash';
import { Component, OnInit, Input } from '@angular/core';
import { _ParseAST } from '@angular/compiler';
import { SponsorService } from 'src/app/services/api/sponsor.service';

declare var $: any;

@Component({
  selector: 'app-sponsor',
  templateUrl: './sponsor.component.html',
  styleUrls: ['./sponsor.component.scss'],
})

export class SponsorComponent implements OnInit {
  @Input('sponsor') sponsor: any;

  constructor(
    private sponsorService: SponsorService,
  ) { }
  public attachments: any[];

  ngOnInit(): void {
    this.attachments = this.setAttachments();
    $('.modal').on('hidden.bs.modal', () => {
      this.removeDesc('sponsorModal_' + this.sponsor.id);
    });
  }

  setAttachments(): any[] {
    if (this.sponsor.contents) {
      return _.map(this.sponsor.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  /**
   * 클릭 시 해당 스폰서 모달에 description을 출력.
   * 리스트를 그리면서 각 개별 모달을 가지는 구조 때문에 description으로 인한 성능 저하 방지
   */
  setDesc(id): void {
    const memberId = localStorage.getItem('cfair') ? JSON.parse(localStorage.getItem('cfair')).id : '';
    const sponsorId = id.split('_')[1];
    this.sponsorService.findOne(sponsorId, memberId).subscribe(res => {
      this.sponsor = res;
      if (this.sponsor.description) {
        document.getElementById(id + 'desc').innerHTML = this.sponsor.description;
      }
    });
  }

  /**
   * 모달 닫힐 때 해당 스폰서의 description을 제거한다.
   * description에 동영상이 재생 중이면 꺼지지 않기때문에 비워줄 필요 있다.
   */
  removeDesc(id): void {
    if (document.getElementById(id + 'desc').innerHTML) {
      document.getElementById(id + 'desc').innerHTML = null;
    }
  }
}

