import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerUstreamComponent } from './player-ustream.component';

describe('PlayerComponent', () => {
  let component: PlayerUstreamComponent;
  let fixture: ComponentFixture<PlayerUstreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerUstreamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerUstreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
