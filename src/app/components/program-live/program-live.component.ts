import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-program-live',
  templateUrl: './program-live.component.html',
  styleUrls: ['./program-live.component.scss']
})
export class ProgramLiveComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('program-live') programLive;

  constructor() { }

  ngOnInit(): void {
  }

}
