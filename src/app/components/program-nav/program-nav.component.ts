import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-program-nav',
  templateUrl: './program-nav.component.html',
  styleUrls: ['./program-nav.component.scss']
})
export class ProgramNavComponent implements OnInit {

  @Output('setDateFn') setDateFn = new EventEmitter();
  @Output('setRoomFn') setRoomFn = new EventEmitter();

  @Input('selected') selected: any;

  @Input('dates') dates: any;
  @Input('rooms') rooms: any;

  constructor() { }

  ngOnInit(): void {

  }

}
