import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from 'src/app/services/api/member.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SocketService } from 'src/app/services/socket/socket.service';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: any;
  public menus: any = [
    { title: 'Welcome', router: '/about' },
    { title: 'Program', router: '/program' },
    { title: 'Live', router: '/live' },
    { title: 'Special VOD', router: '/vod', class: 'VOD' },
    { title: 'Edu Director VOD', router: '/vod2', class: 'VOD' },
    { title: 'Speakers', router: '/speakers' },
    { title: 'Academic Exhibition', router: '/posters' },
    { title: 'E-Booth', router: '/e-booth' },
    // { title: 'Sponsors', router: '/sponsors' },
    { title: 'Notice', router: '/board' },
  ];

  private koreaTimer;
  public koreaTime;
  collapse = true;

  constructor(
    private router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(localStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;
        // console.log(this.user)

        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;
      });
  }

  ngOnInit(): void {
    this.getTime();
  }

  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
  }

  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe(res => { });
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = moment().utc().utcOffset('+0900'); // 한국 시간 : UTC +0900
    }, 1000);
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }
}
